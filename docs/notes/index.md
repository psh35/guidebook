# Notes from the Division

Occasionally we find ourselves writing guides to ourselves or to others about
how we do things and learnings from mistakes we've made. This section of the
guidebook provides a single place for these notes to live.

* The [Automating Google Drive Access](automating-google-drive.md) note
    discusses ways we set up our scripts to interact with Google Drive in an
    automated way without relying on using "real person" credentials.

* The [Webapp Developer Environment](webapp-dev-environment.md) note discusses
    our standard development environment when creating web applications.
