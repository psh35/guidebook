# GPG Public Keys

The [GPG public keyring](../downloads/teampubkeys.gpg) for the DevOps team can
be used to securely send files or emails to us.

## GPG cheat sheet

Here are some handy commands to keep around for using
[GPG](https://en.wikipedia.org/wiki/GNU_Privacy_Guard) to communicate with us.
These instructions assume that you have GPG installed and are relatively
familiar with using the command line.

### Listing contents of the public keyring

Download our [GPG public keyring](../downloads/teampubkeys.gpg) and use the
following command:

```sh
gpg --list-keys --no-default-keyring --keyring ./teampubkeys.gpg
```

### Encrypting a file to send to us

Download our [GPG public keyring](../downloads/teampubkeys.gpg) and use the
following command to encrypt a file named `foo.txt` so that `spqr1@cam.ac.uk`
and `jo.example@uis.cam.ac.uk` can decrypt it:

```sh
gpg --no-default-keyring --keyring ./teampubkeys.gpg --armour \
  --recipient spqr1@cam.ac.uk \
  --recipient jo.example@uis.cam.ac.uk \
  --encrypt foo.txt
```

!!! note
    The recipient email address must match those listed in the public keyring.
    The use of `--no-default-keyring` helps ensure that the key you use for a
    recipient is one from our team keyring and not a personal key.
