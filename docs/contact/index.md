# Contacting the team

The generic email address for the team is
[devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk). We have some
service-specific contact addresses which should be used when contacting us about
the corresponding service:

* GitLab support requests should be [raised as an
    issue](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/issues) on
    the support project in GitLab. Should you need to email the GitLab
    operations team directly, you can used [gitlab-team@developers.cam.ac.uk](mailto:gitlab-team@developers.cam.ac.uk).
* Email about the legacy git.uis service should be directed to
    [gitmaster@uis.cam.ac.uk](mailto:gitmaster@uis.cam.ac.uk).
* Support requests for Raven should be sent to
    [raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk). The Raven
    admin team can be contacted directly *in an emergency* via
    [raven-admin@uis.cam.ac.uk](mailto:raven-admin@uis.cam.ac.uk).

You can send us GPG encrypted files or email by using our [GPG public
keys](./public-keys.md).
