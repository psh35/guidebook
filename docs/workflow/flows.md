# Common flows

This page lists common tasks that one might do as a member of DevOps and the
flows that one does when performing them.

!!! tip
    These flows are far quicker if you make use of [GitLab quick
    actions](https://docs.gitlab.com/ee/user/project/quick_actions.html#quick-actions-for-issues-and-merge-requests)
    for issues and merge requests.

## What shall I do?

If you have nothing else to do, you can find something to do in the following
ways:

* Look at the
    [board](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/boards) and
    [start work](#starting-work-on-an-issue) on an open unclaimed issue.
* Go to the [issues
    list](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/issues) and
    search for open issues, assigned to you with the ``workflow::rework`` label.
    These are issues which you may need to do extra work on.
* Go to the [issues
    list](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/issues) and
    search for open issues, not assigned to you with the ``workflow::review
    required`` label. These are issues which you may be able to review.
* Go to the [issues
    list](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/issues) and
    search for open issues, not assigned to you with the ``workflow::needs
    testing`` label. These are issues which you may be able to test and merge.

## Starting work on an issue

Issues which are on the far left of the board (the "open" column) and which are
unassigned are free for people to pick up. In the [issue life
cycle](./issues.md#issue-life-cycle) these are known as "Open" issues.

1. Select an issue from the board.
1. Assign it to yourself.
1. Add the ``workflow::doing`` label.
1. Work on the issue. If the work involves a code change, see the [making code
   changes](#making-code-changes) flow.
1. When ready for review, add the ``workflow::review required`` label.
1. Add an estimate of the time spent addressing the issue.

## Making code changes

When picking up an issue, it may require code changes in one or more
repositories.

1. In the target repository, make sure ``master`` is up to date:
  ```console
  $ git checkout master
  $ git pull
  ```
1. Create a [feature
   branch](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) in which to
   work. Name the branch ``issue-{issue number}-{summary}``. For example:
  ```console
  $ git checkout -b issue-150-fix-incorrect-docs
  ```
1. Develop the feature.
1. Push the branch back to GitLab:
  ```console
  $ git push -u origin issue-150-fix-incorrect-docs
  ```
1. GitLab will respond with a link to create a merge request for the new branch.
   Visit the link and make sure the merge request is configured in the following
   way:
    * Requires at least one approver.
    * Delete source branch after merging.
1. If your merge request *in and of itself* entirely addresses the issue, you
   can automatically close the issue by adding "Closes #[issue number]" to the
   merge request description. (In GitLab terminology, these are called
   [issue closing
   patterns](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html).)
1. If your merge request helps an issue but does not entirely address it,
   mention the issue but do not use an issue closing pattern. For example: "this
   MR helps us with #[issue number]".
1. Add the ``workflow::review required`` label to the *issue* which the merge
   request addresses. Do *not* add the label to the merge request.

## Reviewing issues

Open issues labelled ``workflow::review required`` are open to be reviewed.

!!! important
    You should always add the time you spend reviewing merge requests to the
    related issue and *not* the merge request.

There are two classes of issues: ones with associated merge requests and ones
without.

### Issues with no related merge request

For issues without a related merge request, you should be able to review the
issue directly.

1. Go to the issue and determine if the work completed addresses the issue.
1. If the work addresses the issue, close the issue.
1. If the work does not address the issue, leave a comment explaining why and
   add the ``workflow::rework`` label.

### Issues with a related merge request

See also the dedicated [merge request page](./merge-requests.md) page for more
information.

!!! warning
    The various ``workflow::...`` labels are used to reflect the progress of the
    *issue* in the board. Merge requests do not appear on the board and so
    **workflow labels should only ever be set on the issue**.

1. Go to the issue and select a related merge request which a) has no assignee
   or b) is assigned to you.
1. Assign yourself to the merge request.
1. Review the code.
1. If you are happy with the code, approve the merge request and add the
   ``workflow::needs testing`` label *to the related issue*.
1. If unhappy, make sure the review describes what should be done, add the
   ``workflow::rework`` label *to the related issue*, assign the merge request
   to the original author and stop work on the merge request for the moment.
1. Test the merge request.
1. If happy, merge.
1. If not happy, explain why, add the ``workflow::rework`` label *to the related
   issue*, assign the merge request to the original author, remove your approval
   of the merge request and stop work on the merge request for the moment.
