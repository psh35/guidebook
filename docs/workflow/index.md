---
title: Our workflow
---

# How we structure our work

This section provides an overview of the day-to-day workflow for a member of the
DevOps division.

## Sprints

We schedule our work in terms of fortnightly *sprints* which run Wednesday to
Wednesday. The middle Wednesday is reserved for [backlog
refinement](issues.md#issue-refinement) and the final Wednesday is reserved
for [scheduling the next sprint](issuemd#effort-estimation).

## Learning resources

If some of the technologies and tools referenced in this document are
unfamiliar, the following resources may be useful:

* [GitLab documentation](https://docs.gitlab.com/ee/README.html).
* [GitLab](https://www.linkedin.com/learning/continuous-delivery-with-gitlab)
    course on LinkedIn Learning.
* [Git for teams](https://www.linkedin.com/learning/git-for-teams) course on
    LinkedIn Learning.
* [Git-related courses](https://www.linkedin.com/learning/topics/git) on
    LinkedIn Learning.
