// HTTP->HTTPS redirect is not yet directly supported by RTD
// (https://github.com/rtfd/readthedocs.org/issues/4641). As such we have to use
// somewhat hacky tricks like this to do client-side redirect from HTTP->HTTPS.
if (location.protocol != 'https:' && location.hostname != 'localhost' && location.hostname != '127.0.0.1') {
  location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}
