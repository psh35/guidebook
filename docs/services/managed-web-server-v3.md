# Managed Web Service - Version 3

This page documents key information about version 3 of the Managed Web Service.

Environments and Servers they run on
------------------------------------

- [Production](https://panel.mws3.csx.cam.ac.uk/)
    - The panel VM is mws3-live-panel1.srv.uis.private.cam.ac.uk
    - The VM cluster is composed by the nodes that you can find in https://github.com/uisautomation/mws-ansible/blob/master/production
- [Test](https://test.dev.mws3.csx.cam.ac.uk/) 
    - The panel VM is test.dev.mws3.csx.cam.ac.uk
    - The VM cluster is composed by the nodes that you can find in https://github.com/uisautomation/mws-ansible/blob/master/test

Application repositories
------------------------
- [MWS Web Control Panel](https://github.com/uisautomation/mws-panel/). Django web panel for controling MWS sites.

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Web Panel | Python 2.7 | Django 1.11 |
| Infrastructure Deployment | - | Ansible |
| Virtual Machines | - | Xen |

Deployment
----------
Updates to the Web panel can be done via mws-ansible repo referenced below. Updates to the rest of the infrastructure can also be deployed the same report.
The README of the repo should include information about how to deploy to different parts of the infrastructure.

Deployment repository
---------------------
- [MWS Ansible](https://github.com/uisautomation/mws-ansible). Ansible repository for all components of the MWS infrastructure.
- [MWS Misc](https://gitlab.developers.cam.ac.uk/uis/devops/mws/mwsv3/mws-misc). DNS API (in jackdaw), preseed files, nagios configuration, and other scripts.

Service Owner
-------------
[Jon Holgate](https://www.lookup.cam.ac.uk/person/crsid/jh535)

Service Managers
----------------
[Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)

Current Status
--------------
Live

Documentation
-------------
- [Managed Web Server 3 UIS wiki](https://wiki.cam.ac.uk/uis/MWSv3).
- [Managed Web Server 3 UCS wiki](https://wiki.cam.ac.uk/ucs/Managed_Web_Server_version_3). Some of it may be out of date but is more complete than the UIS wiki.
