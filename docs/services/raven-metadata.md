# Raven Metadata

This page documents key information about the Raven Metadata service.

Environments and Servers they run on
------------------------------------

- [Production](https://metadata.raven.cam.ac.uk/)
    - sa-live-usvc{1-3}.srv.uis.private.cam.ac.uk ([usvc swarm](https://portainer.swarm.usvc.gcloud.automation.uis.cam.ac.uk/))
    - [GCP uis-microservices (SQL DB shib_metadata)](https://console.cloud.google.com/sql/instances/sql-6e3f03c1/databases?project=uis-microservices)
- [Test](https://shibmetadata-test.swarm.usvc.gcloud.automation.uis.cam.ac.uk/)
    - sa-live-usvc{1-3}.srv.uis.private.cam.ac.uk ([usvc swarm](https://portainer.swarm.usvc.gcloud.automation.uis.cam.ac.uk/))
    - [GCP uis-microservices (SQL DB shib_metadata-test)](https://console.cloud.google.com/sql/instances/sql-6e3f03c1/databases?project=uis-microservices)

Application repositories
------------------------
- [Web Application](https://gitlab.developers.cam.ac.uk/uis/devops/raven/shibboleth-metadata)

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Python 3.7 | Django 2.1 |
| Client | Javascript | jQuery 1.11.1 <br> d3js 5.9.2 <br> c3js 0.6.14 |

Deployment
----------
**Terraform** for GCP project and SQL instance creation. **Ansible** for docker
swarm configuration.

Deployment repository
---------------------
- [USVC Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/usvc-deploy)

Service Owner
-------------
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

Service Managers
----------------
[Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)

Current Status
--------------
Live

Documentation
-------------
- [Main documentation](https://wiki.cam.ac.uk/raven/Shibboleth_documentation_and_HOWTOs)
