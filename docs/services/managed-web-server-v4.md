# Managed Web Service - Version 4

This page documents key information about version 4 of the Managed Web Service.

Environments and Servers they run on
------------------------------------
- Production
    - No application at present
    - [OpenStack dashboard](https://vss.cloud.private.cam.ac.uk)
    - OpenStack project: `rcc-amc203-project-ca1fcb7a`
    - [OpenStack API Endpoint](https://vss.cloud.private.cam.ac.uk:5000/v3)
- Test
    - No application at present
    - [OpenStack dashboard](https://vss.cloud.private.cam.ac.uk)
    - OpenStack project: `rcc-amc203-devops-poc`
    - [OpenStack API Endpoint](https://vss.cloud.private.cam.ac.uk:5000/v3)

Application repositories
------------------------
- [MWSv4 Panel](https://gitlab.developers.cam.ac.uk/uis/devops/mws/v4/panel)

Technology
----------
| Category | Language | Framework |
| -------- | -------- | --------- |
| Panel | Python 3.7 | Django 2.2 |
| Client | JavaScript | jQuery v2.1.1 |
| Infrastructure | Terraform | N/A |
| Infrastructure | Ansible | N/A |

Deployment
----------
Bootstraping MWSv4

- Get an Openstack tennancy from HPC
- Run the MWSv4 infra terraform over the blank tennancy
- Run the [Ansible for the panel](https://gitlab.developers.cam.ac.uk/uis/devops/mws/v4/infra) 
  against the panel VM created by Terraform

Deployment repository
---------------------
- [MWSv4 Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/mws/v4/infra)

Service Owner
-------------
[Jon Holgate](https://www.lookup.cam.ac.uk/person/crsid/jh535)

Service Managers
----------------
[Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)

Current Status
--------------
Planning

Documentation
-------------
- [MWSv4 Wiki](https://gitlab.developers.cam.ac.uk/uis/devops/mws/v4/panel/wikis/home)
- [OpenStack SDK](https://docs.openstack.org/openstacksdk/latest/)
