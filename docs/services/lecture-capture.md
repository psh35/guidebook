# Lecture Capture

This page documents key information about the Lecture Capture service.

Environments and Servers they run on
------------------------------------

- [Production](https://admin.lecturecapture.uis.cam.ac.uk) 
    - lecturecapture-chost1.srv.uis.private.cam.ac.uk
    - lecturecapture-chost2.srv.uis.private.cam.ac.uk
    - lecturecapture-chost3.srv.uis.private.cam.ac.uk
- [Test](https://test-admin.lc.gcloud.automation.uis.cam.ac.uk) 
    - lecturecapture-test-chost1.srv.uis.private.cam.ac.uk
    - lecturecapture-test-chost2.srv.uis.private.cam.ac.uk
    - lecturecapture-test-chost3.srv.uis.private.cam.ac.uk

Application repositories
------------------------
- [Gitlab Lecture Capture project](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture)

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Orchestration: Opencast 6 | Java ||
| Capture Agent: (Galicaster) | python |  |
| Preferences app | python | Django |

Deployment
----------
Backend - deployed with [docker containers](https://github.com/opencast/opencast-docker) on locally hosted docker swarm with [ansible](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/lecture-capture-backend)
. Also uses an AWS RDS database deployed with terraform from the [Lecture Capture Backend](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/lecture-capture-backend) repo.

Capture Agents - ([Galicaster](https://github.com/teltek/Galicaster)) - deployed by [OS image](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/agent-bootstrap) and then [ansible](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/agent-ansible)

Deployment repository
---------------------
- [Lecture Capture Backend](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/lecture-capture-backend) (including AWS RDS terraform)
- [Capture Agents](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/agent-ansible)

Service Owner
-------------
[Dr A.D.J.Scadden](https://www.lookup.cam.ac.uk/person/crsid/adjs100)

Service Managers
----------------
[Stephen Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)

Current Status
--------------
Planning

Documentation
-------------
- [General Documentation](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/docs)
