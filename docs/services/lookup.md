# Lookup

This page documents key information about the Lookup service.

Environments and Servers they run on
------------------------------------

- [Production webapp](https://www.lookup.cam.ac.uk/)
    - Two LDAP endpoints for HA configurations: ldap{1,2}.lookup.cam.ac.uk.
    - LDAP endpoint pointing to "live" node: ldap.lookup.cam.ac.uk.
    - Hosted on {dingo,wolf}.csi.cam.ac.uk.
- [Test webapp](https://lookup-test.csx.cam.ac.uk/)
    - LDAP endpoint pointing to "test" node: lookup-test.csx.cam.ac.uk.
    - Hosted on jodrell.csx.cam.ac.uk.

Application repositories
------------------------

- [ibis repo](https://subversion.csi.cam.ac.uk/cgi-bin/viewvc.cgi/identity-management/ibis/) (SVN)

Technology
----------

| Category | Language    | Framework |
| -------- | ----------- | --------- |
| Server   | Java+Groovy | Grails    |

Deployment
----------

RPMs

Deployment repository
---------------------

[Bes RPM repository](https://bes.csi.cam.ac.uk/yum/)

Service Owner
-------------

* [Vijay Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1)

Service Managers
----------------

* [Currently vacant](https://www-falcon.csx.cam.ac.uk/site/UISINTRA/how/itsm/service-management-list)

Current Status
--------------

Live

Documentation
-------------

- [UIS help site documentation](https://help.uis.cam.ac.uk/service/collaboration/lookup)
- [Web service API](https://help.uis.cam.ac.uk/service/collaboration/lookup/ws)
