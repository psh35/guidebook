# Raven (SAML2)

Raven is the web authentication service for the University. This page documents
the SAML2 personality for the Raven service.

This personality is also known as "Shibboleth" within the [UK Access Management
Federation](https://www.ukfederation.org.uk/).

Terminology
-----------

* **Service Provider** (SP) - website using Raven/SAML2 to authenticate its
    users.
* **Identity Provider** (IdP) - the Raven/SAML2 service.

Environments
------------

- [Production](https://shib.raven.cam.ac.uk/) - should be used by all production
    SPs.
- [Next](https://shib-next.raven.cam.ac.uk/) - should be used by some test SPs
    (see below).

!!! note
    The "shib-next" IdP represents the current ``master`` branch of the Ansible
    playbook and as such is what the live service will be next time it is
    deployed. Users wishing to track development to get early warning of issues
    with their SP may do so by adding the following line to their ``/etc/hosts``
    file:

        128.232.132.17 shib.raven.cam.ac.uk shib-next.raven.cam.ac.uk

    This should be done on their local client, i.e. the machine they are running
    their web browser on.

The individual VMs hosting the service are
[listed](https://gitlab.developers.cam.ac.uk/uis/devops/raven/operational-documentation/wikis/shib/List-of-VMs)
in the Operational Documentation. (DevOps only)

Technology
----------

!!! warning
    This service uses a technology stack which does not align with DevOps
    Division recommendations.

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Java | Spring web framwork |

Deployment
----------

Deployment is via Ansible playbook to an on-premise set of servers. See the
[operational
documentation](https://gitlab.developers.cam.ac.uk/uis/devops/raven/operational-documentation)
project (DevOps only) for more information.

Deployment repository
---------------------

- [Ansible deployment](https://gitlab.developers.cam.ac.uk/uis/devops/raven/ansible-shibboleth) (DevOps only)

Service Owner
-------------
[Vijay Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1)

Service Managers
----------------
[Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)

Current Status
--------------

Live

Documentation
-------------

!!! note
    Raven documentation is in the process of being rationalised into a single
    place.

- [Raven wiki](https://wiki.cam.ac.uk/raven/Main_Page)
- [Project page](https://raven.cam.ac.uk/project/)
- [UCS Wiki pages](https://wiki.cam.ac.uk/ucs/Shibboleth) (UIS only)
