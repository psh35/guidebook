# Streaming Media Service

This page documents key information about the Streaming Media Service service.

Environments and Servers they run on
------------------------------------

- [Production](https://sms.cam.ac.uk) 
    - sms-node1.sms.cam.ac.uk (web app and DB)
    - sms-node2.sms.cam.ac.uk (web app and DB)
    - smew.sms.csx.cam.ac.uk (downloads)
    - goldeneye.sms.csx.cam.ac.uk (downloads)
    - sms-nfs1.sms.private.cam.ac.uk (ceph->nfs gateway)
    - sms-nfs2.sms.private.cam.ac.uk (ceph->nfs gateway)
    - ruddy.sms.csx.cam.ac.uk (nfs server)
    - transcode1.sms.private.cam.ac.uk (transcoding)
    - transcode2.sms.private.cam.ac.uk (transcoding)
    - transcode3.sms.private.cam.ac.uk (transcoding)
    - transcode4.sms.private.cam.ac.uk (transcoding)
    
- [Test](https://sms-dev.sms.csx.cam.ac.uk/)
    - daffy.sms (web app and DB)
    - donald.sms (web app and DB)
    - sms-dev-storage1 (nfs)
    - sms-dev-storage2 (nfs)
    - mallard.sms (downloads)
    - transcode5.sms.private.cam.ac.uk (transcoding)
     
Application repositories
------------------------
- [SMS subversion repo](https://subversion.csi.cam.ac.uk/cgi-bin/viewvc.cgi/devgroup/sms/trunk/grails-app/)

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Web application | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | 10.5 |

Deployment
----------
Deployed by installing RPMs from the [Bes repo](https://wiki.cam.ac.uk/ucs/Bes_-_Managed_Installation_Service#.28S.29RPMS_Available_on_Bes)

Deployment repository
---------------------
- [bes.csi.cam.ac.uk](http://bes.csi.cam.ac.uk/)

Service Owner
-------------
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

Service Managers
----------------
[Stephen Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)

Current Status
--------------
Live

Documentation
-------------
- [UCS Wiki](https://wiki.cam.ac.uk/ucs/SMS_-_Streaming_Media_Service)
- [Gitlab wiki](https://gitlab.developers.cam.ac.uk/uis/devops/sms/docs/wikis/Legacy-SMS-Docs)
